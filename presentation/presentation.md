---
title:
- Auditory Spatial Augmented Reality for Group Immersion
author:
- Nicolas Bouillot
institute:
- Société des arts technologiques
theme:
- moon
date:
- 03 October 2022
aspectratio:
- 169
navigation:
- empty
section-titles:
- false
output:
  revealjs::revealjs_presentation:
    css: styles.css
---

# Presentation structure

* [SAT] Metalab open source tools
* Group immersion equation (tentative)
* Group immersion as always-on Mixed reality ?

# [SAT] Metalab tools

open source and interoperable !

# Our (audio) hardware

![](img/audio-wiredome.jpg){width=40%}
![](./img/audiodice-osm-sato.jpg){width=40%}

Satosphère & Audiodice

# Our (audio) software

SATIE <br> <em>audio spatialization engine developed for realtime rendering of dense audio scenes to large multichannel loudspeaker systems</em>

vaRays <br> <em>realtime Impulse Response engine for virtual acoustic of 3D environment</em>

# Group immersion equation (tentative)

(places + group of people)
<br> x <br>
(sensitive + social + situated + interactive)
<br> = <br>
group immersion

# Sensitive

perception of being physically present in a (re-)created world

#

<iframe src="https://player.vimeo.com/video/522509890?color=ffffff&title=0&byline=0&portrait=0" width="720" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/522509890">6DoF symphonic navigation</a></a></p>

Credits: Settel, Z.; Munch, J.; and Bouillot, N.

# Situated

space is augmented with spatial artifacts

#
<iframe src="https://player.vimeo.com/video/290925507?h=af4b922189" width="720" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/290925507">Scalable Haptic Floor</a></p>

Credits: Bouillot N.; Seta M.; Gravel S.; Brault V.; Bennacer M.; Generique Design; and St-Arnault L.P.

# Interactive
interaction among the group, and possibly with the created world

#
<iframe src="https://player.vimeo.com/video/484122810?h=8943063b2b" width="720" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/484122810">Live Acoustic Simulation with vaRays</a></p>

Credits: Ouellet-Delorme, É.; Venkatesan, H.; Durand, E.; Bouillot, N. and Seta M.

# Social

communication-enabled group of participants

#
<iframe src="https://player.vimeo.com/video/664359535?h=d1315759fc" width="720" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/664359535">Motus Domum Residency</a></p>

Credits: Settel Z.; Zeki O.; and Trotzmer P.

# Group Immersion as always-on mixed reality ?

* ✅ physical spaces are always-on
* ✅ overlays virtual and real-world elements
* 🤝 complementary to "body-equiped" mixed-reality approaches

# Let's collaborate !

* 6DoF audio, multimodal creation, live acoustics and embedded systems for audio display 
* open source contribution
* internships

# Links

* Website: [https://sat-mtl.gitlab.io/en/](https://sat-mtl.gitlab.io/en/)
* Internships : [https://sat-mtl.gitlab.io/metalab/lab/categories/internships/](https://sat-mtl.gitlab.io/metalab/lab/categories/internships/)
* SATIE: [https://sat-mtl.gitlab.io/documentation/satie/en/](https://sat-mtl.gitlab.io/documentation/satie/en/)
* vaRays: [https://gitlab.com/sat-mtl/tools/vaRays](https://gitlab.com/sat-mtl/tools/vaRays)
* Audiodice: [https://gitlab.com/sat-mtl/tools/audiodice](https://gitlab.com/sat-mtl/tools/audiodice)

nbouillot@sat.qc.ca
